from loops.Attacks import AttackTypes
from loops.Loops import LoopAttack
import foolbox
import keras
import numpy as np
from keras.applications.resnet50 import ResNet50
import os
import time
import json
from keras.preprocessing import image
from scipy.misc import imsave
import random

class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """
    def default(self, obj):
        if isinstance(obj, (np.int_, np.intc, np.intp, np.int8,
            np.int16, np.int32, np.int64, np.uint8,
            np.uint16, np.uint32, np.uint64)):
            return int(obj)
        elif isinstance(obj, (np.float_, np.float16, np.float32,
            np.float64)):
            return float(obj)
        elif isinstance(obj,(np.ndarray,)):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


def saveResults(data,savePath):
    print("Saving results...")
    os.makedirs(savePath)
    for item in data:
        itemPath = savePath + "/" + str(item["loop"])
        os.makedirs(itemPath)
        if item["input"] is not None:
            imsave(itemPath + "/original.png", item["input"])
        if item["adversarialExample"] is not None:
            imsave(itemPath + "/adv.png", item["adversarialExample"])
        if item["labelAdversarial"] is None:
            item["labelAdversarial"] = -1
        if item["labelOriginal"] is None:
            item["labelOriginal"] = -1
        executionDetails = {
            "loop": item["loop"],
            "originalLabel": int(item["labelOriginal"]),
            "attack": str(item["attack"]),
            "advLabel": int(item["labelAdversarial"])
        }
        with open(itemPath + "/executionDetails.json", 'w') as exresults:
            json.dump(executionDetails, exresults)


# instantiate model
keras.backend.set_learning_phase(0)
kmodel = ResNet50(weights='imagenet')
loopAttack = LoopAttack(model=kmodel,bounds=(0,255),loops=100)

path = "./loopsdat"
seenJsonPath = "./res/seen.json"
seenJsonOut = "./res/seen.json"
outPath = "./output/dat"
with open(seenJsonPath) as f:
    seen = json.load(f)
excludedAttacks = [AttackTypes.BoundaryAttack, AttackTypes.IterativeGradientAttack, AttackTypes.PointwiseAttack,AttackTypes.IterativeGradientSignAttack,AttackTypes.IterativeGradientAttack,AttackTypes.SLSQPAttack,AttackTypes.ApproximateLBFGSAttack]
categories = os.listdir(path)

total = 0
for category in categories:
    images = os.listdir(path + "/" + str(category))
    print("Category: " + str(category), " with ", str(len(images)) + " images.")
    total+=len(images)

x = 0
while x <= total:
    category = random.choice(list(categories))
    images = os.listdir(path + "/" + str(category))
    img = random.choice(list(images))
    y=0
    while (str(img) in seen["seen"]) and (y <= len(images)):
        img = random.choice(list(images))
        y+=1
    if str(img) in seen["seen"]:
        continue
    TargetImg = image.load_img(path  + "/" + category + "/" + img, target_size=(224, 224))
    TargetImg = image.img_to_array(TargetImg)
    advData = loopAttack.all_except_random(data=TargetImg, excludedAttacks=excludedAttacks)
    seen["seen"].append(str(img))
    saveResults(advData,outPath + "/" + str(category) + "/" + str(img))
    with open(seenJsonOut, 'w') as outfile:
        json.dump(seen, outfile)
    x+=1


